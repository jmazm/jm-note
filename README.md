# jm-note

## clone项目

```
由于有submodule，需要recursive

git clone git@gitlab.com:jmazm/life-note.git --recursive
```

## 笔记

* 前端笔记

```
cd font-end-note

.....

git add
git commit
git push origin HEAD:<name-of-remote-branch>
```

* 其他笔记

```
cd life-note

.....

git add
git commit
git push origin HEAD:<name-of-remote-branch>
```